$(document).ready(function () {
    $('.menu-btn, .backdrop').on('click', function () {
        $('.sidebar').toggleClass('active');
    });
    $('[data-toggle="tooltip"]').tooltip()
});

// When the user clicks on <div>, open the popup
function popupFn() {
  var popup = document.getElementById("myPopup");
  popup.classList.toggle("show");
}

// function to check online and offline internet
function updateOnlineStatus() {
  document.getElementById("status").innerHTML = "User is online";
}

// function online offline status
function updateOfflineStatus() {
  document.getElementById("status").innerHTML = "User is offline";
}

window.addEventListener('online', updateOnlineStatus);
window.addEventListener('offline', updateOfflineStatus); 
